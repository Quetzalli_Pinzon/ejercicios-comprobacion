import { LitElement, html } from 'lit-element';

class MyElement extends LitElement {
  static get properties() {
    return {
        message: {type: String},
      anotherArray: { type: Array},
      myArray: { type: Array },
      myBool: { type: Boolean }
    };
  }
  constructor() {
    super();
    this.message = "Componente my-element"; 
    this.anotherArray = ['Soy','otro','array','de','prueba'];
    this.myArray = ['Soy','un','array','de','prueba'];
    this.myBool = true;
  }
  render() {
    return html`
      <p>${this.message}</p>
      <ul>
        ${this.anotherArray.map(item => html`<li>${item}</li>`)}
      </ul>
      ${this.myBool?
        html`<p>Renderizado de HTML si myBool es true</p>`:
        html`<p>Renderizado de HTML si myBool es false</p>`}
    `;
  }
}

customElements.define('my-element', MyElement);