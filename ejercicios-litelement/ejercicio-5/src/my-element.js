import { LitElement, html } from 'lit-element';

class MyElement extends LitElement {
  static get properties() {
    return {
       title: {type: String},
    };
  }

  constructor(){
    super();
    this.title = "Este es mi componente"
  }

  render() {
    return html`
      <p>${this.title}</p>`;
  }


createRenderRoot(){
  return this;
}
}


window.customElements.define('my-element', MyElement);