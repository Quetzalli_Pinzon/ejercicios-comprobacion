import { LitElement, html } from 'lit-element';

class MyComponente extends LitElement {
  static get properties() {
    return {
       label: {type: String},
       uid: {type: String}
    };
  }

  render() {
    return html`
      <input type ="checkbox" id=${this.uid}>${this.label}`;
  }
}

window.customElements.define('my-componente', MyComponente);