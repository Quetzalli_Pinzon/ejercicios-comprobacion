function bar(foo) {
    console.log( foo && foo * 3 || null);
}

bar(5); // En consola 15
bar(2); // En consola 6
bar();  // En consola null